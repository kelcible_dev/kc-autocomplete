# README #

### What is this repository for? ###

* C'est un module qui permet de gérer l'autocompletion facilement sur un input voulu à un ou plusieurs endroit(s) dans le site. L'input est généré par le shortcode [kc_autocomplete]. 
* Le module utilise la bibliothèque JS: Awesomplete. 

* Version 1.0
* Auteur : Loïc Libert (Kelcible)

### How do I get set up? ###

* Le plugin s'installe en copiant les dossiers dans le dossier plugin de votre Wordpress.
* Sur la page/ le post inséré le shortcode [kc_autocomplete] qui prend en paramètre : 
*	- post_query = "post, page, custom" (valeur initiale) : Correspond au type de recherche effectuée la valeur "meta" est également possible mais doit être rentrer en paramètre unique de post_query 
*															contrairement à "post", "page" et "custom" qui peuvent être rentrés seuls ou en groupe.
*															"post" effectue une recherche sur les articles
*															"page" effectue une recherche sur les pages
*															"custom" effectue une recherche sur les customs posts
*															"meta" effectue une recherche sur les meta-values et y associe le titre du post/de la page correspondant(e) pour plus de précision
* 
*	- min_letters = 3 (valeur initiale) : Correspond au nombre de lettres minimales que l'utilisateur devra taper avant que l'autocompletion se lance.
*	- id = "" (valeur initiale) : Attribue un id à votre input pour le manipuler côté CSS/JS.
* 
* Pour le moment vous pouvez mettre jusqu'à 3 inputs par page (plus nécessite un ajustement du code).
* Le champs autocomplété prend en valeur le titre du post/page/meta_value recherché et un input caché prend en valeur l'id associé au post/page.

### Contribution guidelines ###

### Who do I talk to? ###

* Ce plugin a été développé en interne et est prévu pour une utilisation pour l'agence Kelcible.