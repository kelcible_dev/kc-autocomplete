<?php

/** 
 * Plugin Name: KC autocomplete
 * Author: Loïc LIBERT
 * Description: Création d'un module Kelcible qui permet de gérer l'autocompletion facilement sur un input voulu à un ou plusieurs endroit(s) dans un site.
 * Version: 1.0
 */


// Appel des différents scripts (bibliothèque awesomplete, script.js, style.css et requête AJAX).
function add_plugin_scripts() {
    wp_enqueue_script('awesompletejs', plugins_url('/awesomplete/awesomplete.js', __FILE__));
    wp_enqueue_style('awesompletecss', plugins_url('/awesomplete/awesomplete.css', __FILE__));
    wp_enqueue_script('kc-autocomplete', plugins_url( '/js/kc-autocomplete.js', __FILE__));
    wp_enqueue_style('kc-autocompletecss', plugins_url('/css/kc-autocomplete.css', __FILE__));

    wp_localize_script('kc-autocomplete', 'kc_autocomplete_script', array('ajax_url' => admin_url('admin-ajax.php')));
    wp_localize_script('kc-autocomplete', 'autocomplete_hidden_id', array('ajax_url' => admin_url('admin-ajax.php')));
    wp_localize_script('kc-autocomplete', 'select_meta', array('ajax_url' => admin_url('admin-ajax.php')));
 }
add_action('wp_enqueue_scripts', 'add_plugin_scripts');


// envoi de la requête SQL et récupération dans un tableau; 
function get_autocomplete(){
    global $wpdb;
    
    if(isset($_POST['search_query']) && isset($_POST['post_query'])){
        $datas = explode(',', $_POST['post_query']);
        foreach ($datas as &$data) {
            $data = trim($data);
        }
        unset($data);

        $input = $wpdb->esc_like(stripslashes($_POST['search_query']));

        // Vérif que la string tapée est contenue n'importe où dans les titres
        $input_contains = '%' . $input . '%';
        
        // Vérif des différents paramètres entrés et envoi d'une requête SQL adaptée
        if(count($datas) == 1 && $datas[0] != "meta" && $datas[0] != "custom"){ // 1 seul param autre que "custom" et "meta";
            $sql = "SELECT * FROM $wpdb->posts WHERE post_title LIKE %s AND post_type=%s AND post_status='publish'";
            $sql = $wpdb->prepare($sql, array($input_contains, $datas[0]));

        } else if(count($datas) == 1 && $datas[0] == "custom"){ // 1 seul param "custom";
            $sql = "SELECT * FROM $wpdb->posts WHERE post_title LIKE %s AND post_type!='post' AND post_type!='page' AND post_status='publish'";
            $sql = $wpdb->prepare($sql, $input_contains);

        } else if(count($datas) == 2 && $datas[0] != "meta" && $datas[1] != "meta" && $datas[0] != "custom" && $datas[1] != "custom"){ // 2 params autres que "custom" et "meta"
            $sql = "SELECT * FROM $wpdb->posts WHERE post_title LIKE %s AND post_type=%s AND post_status='publish' OR post_type=%s AND post_status='publish'";
            $sql = $wpdb->prepare($sql, array($input_contains, $datas[0], $datas[1]));

        } else if(count($datas) == 2 && $datas[0] != "meta" && $datas[1] != "meta" && $datas[0] == "custom" || $datas[1] == "custom"){ // 2 params dont "custom"
            $sql = "SELECT * FROM $wpdb->posts WHERE post_title LIKE %s AND post_type=%s AND post_status='publish' OR post_type!='page' AND post_type!='post' AND post_status='publish'";

            if($datas[0] == "custom"){$sql = $wpdb->prepare($sql, array($input_contains, $datas[1]));} 
            else if($datas[1] == "custom"){$sql = $wpdb->prepare($sql, array($input_contains, $datas[0]));}

        } else if(count($datas) == 3 && $datas[0] != "meta" && $datas[1] != "meta" && $datas[2] != "meta"){ // les 3 params sont selectionnés
            $sql = "SELECT * FROM $wpdb->posts WHERE post_title LIKE %s AND post_status='publish'";
            $sql = $wpdb->prepare($sql, $input_contains);

        } else if(count($datas) == 1 && $datas[0] == "meta"){ // le param est le fameux "meta"
            $sql = "SELECT * FROM $wpdb->postmeta as a INNER JOIN $wpdb->posts as b ON a.post_id = b.ID WHERE meta_value LIKE %s AND post_status='publish'";
            $sql = $wpdb->prepare($sql, $input_contains);

        }
        $results = $wpdb->get_results($sql);
        $post_data = array();

        if(count($datas) == 1 && $datas[0] == "meta"){
            foreach ($results as $result){
                $post_data[] = array(
                    'label' => addslashes($result->meta_value). "</br><span class='fildArianne'>" . addslashes($result->post_title)."</span>" ,
                    'value' => addslashes($result->meta_value),
                );
            }
        } else {
            foreach ($results as $result){
                $post_data[] = array(
                    'label' => addslashes($result->post_title),
                    'value' => addslashes($result->post_title),
                );
            }
        }
        // Array de datas correspondantes à la recherche
        echo json_encode($post_data);
    }

    wp_die();
}
add_action('wp_ajax_autocomplete_data', 'get_autocomplete');
add_action('wp_ajax_nopriv_autocomplete_data', 'get_autocomplete');

// Récupère l'id du post/page via le titre autocomplété et le stock dans un input caché
function autocompleted_id(){
    global $wpdb;

    if(isset($_POST['post_title'])){
        $input = $_POST['post_title'];

        $sql = "SELECT ID, post_id FROM $wpdb->posts, $wpdb->postmeta WHERE post_title = %s OR meta_value=%s";

        $sql = $wpdb->prepare($sql, $input, $input);

        $results = $wpdb->get_results($sql);


        $hidden_id = $results[0]->ID;
        echo json_encode($hidden_id);
    }
    wp_die();
}
add_action('wp_ajax_autocompleted_id', 'autocompleted_id');
add_action('wp_ajax_nopriv_autocompleted_id', 'autocompleted_id');

// mon shortcode paramétrable avec les options : post_type = "post/page/custom/meta", ..., ...
function kc_autocomplete_shortcode($atts) {
    $a = shortcode_atts( array(
        'id' => '',
        'post_query' => 'post, page, custom',
        'min_letters' => 3
    ), $atts);

    $shortcode = "";
    $shortcode.= "<div class='kc-autocomplete' style='height: 500px'>
                        <div class='autocomplete-is-here' style='width: 100%'>
                            <input class='hidden-field' type='hidden' name='id' style='width: 0; height: 0; padding:0; border:0' value=''/>
                            <label for='{$a['id']}'>Recherche en autocompletion</label>  
                            <input id='{$a['id']}' type='text' data-post-query='{$a['post_query']}' data-min-letters='{$a['min_letters']}' name='post-title' />
                        </div>
                    </div>";

    return $shortcode;
}
add_shortcode( "kc_autocomplete", "kc_autocomplete_shortcode" );       

?>