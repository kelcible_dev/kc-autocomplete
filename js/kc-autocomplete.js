document.addEventListener('DOMContentLoaded', function(){
    console.log('kc-autocomplete.js loaded !');

    var ajax = new XMLHttpRequest();

    var mesDivs = document.getElementsByClassName('autocomplete-is-here');
    var autocomplete_field = mesDivs[0].lastElementChild;
    var awesomplete_field = new Awesomplete(autocomplete); 

    var min_letters = parseInt(autocomplete_field.dataset.minLetters); // combien de lettres avant que la requête AJAX se lance.
    
    autocomplete_field.addEventListener('keyup', () => {
        
        var post_query = autocomplete_field.dataset.postQuery;
        var search_query = autocomplete_field.value;

        if(search_query.length >= min_letters) {
            // On lance la requête AJAX
            ajax.open("POST", kc_autocomplete_script.ajax_url);
            ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajax.send('action=autocomplete_data&search_query=' + search_query + '&post_query=' + post_query);
        }
        
    });
    
    // Quand on récupère la réponse d'AJAX
    ajax.onload = function() {
        var json_list = JSON.parse(ajax.responseText);
        awesomplete_field.list = json_list; // Mis à jour de la list Awesomplete
        awesomplete_field.evaluate(); // et on préviens Awesomplete qu'elle l'a été (updated)
    };
    
    // Récup de l'ID du post autocompleté et envoi dans un input caché
    var ajaxHidden = new XMLHttpRequest();
    
    var awesomplete_list = document.getElementById('awesomplete_list_1');
    awesomplete_list.addEventListener('click', () => {
        autocompleted_field = autocomplete_field.value

        ajaxHidden.open("POST", autocomplete_hidden_id.ajax_url);
        ajaxHidden.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajaxHidden.send('action=autocompleted_id&post_title=' + autocompleted_field); 
    });
    
    var hidden_field = mesDivs[0].firstElementChild;
    ajaxHidden.onload = function() {
        var json_response = JSON.parse(ajaxHidden.responseText);
        hidden_field.value = json_response;
    };
    
    if (mesDivs.length > 1) {
            // ------------------------------------------------------------------------------------------ deuxième champ injecté
            var ajax2 = new XMLHttpRequest();

            var autocomplete_field2 = mesDivs[1].lastElementChild;
            var awesomplete_field2 = new Awesomplete(autocomplete_field2);    

            var min_letters2 = parseInt(autocomplete_field2.dataset.minLetters); // combien de lettres avant que la requête AJAX se lance.
            
            autocomplete_field2.addEventListener('keyup', () => {
                
                var post_query = autocomplete_field2.dataset.postQuery;
                var search_query = autocomplete_field2.value;
                if(search_query.length >= min_letters2) {
                    // On lance la requête AJAX
                    ajax2.open("POST", kc_autocomplete_script.ajax_url);
                    ajax2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    ajax2.send('action=autocomplete_data&search_query=' + search_query + '&post_query=' + post_query);
                }
        
            });

            // Quand on récupère la réponse d'AJAX
            ajax2.onload = function() {
                var json_list = JSON.parse(ajax2.responseText);
                awesomplete_field2.list = json_list; // Mis à jour de la list Awesomplete
                awesomplete_field2.evaluate(); // et on préviens Awesomplete qu'elle l'a été (updated)
            };

            // Récup de l'ID du post autocompleté et envoi dans un input caché
            var ajaxHiddenBis = new XMLHttpRequest();
            
            var awesomplete_list2 = document.getElementById('awesomplete_list_2');
            var hidden_field2 = mesDivs[1].firstElementChild;
            awesomplete_list2.addEventListener('click', () => {
                var autocompleted_field = autocomplete_field2.value;
            
                ajaxHiddenBis.open("POST", autocomplete_hidden_id.ajax_url);
                ajaxHiddenBis.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                ajaxHiddenBis.send('action=autocompleted_id&post_title=' + autocompleted_field); 
            });
        
            ajaxHiddenBis.onload = function() {
                var json_response = JSON.parse(ajaxHiddenBis.responseText);
                hidden_field2.value = json_response;
            };
    }
    
    if (mesDivs.length > 2) {
            // ------------------------------------------------------------------------------------------ troisième champ injecté
            var ajax3 = new XMLHttpRequest();

            var autocomplete_field3 = mesDivs[2].lastElementChild;
            var awesomplete_field3 = new Awesomplete(autocomplete_field3);  

            var min_letters3 = parseInt(autocomplete_field3.dataset.minLetters); // combien de lettres avant que la requête AJAX se lance.
            
            autocomplete_field3.addEventListener('keyup', () => {
                
                var post_query = autocomplete_field3.dataset.postQuery;
                var search_query = autocomplete_field3.value;

                if(search_query.length >= min_letters3) {
                    // On lance la requête AJAX
                    ajax3.open("POST", kc_autocomplete_script.ajax_url);
                    ajax3.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    ajax3.send('action=autocomplete_data&search_query=' + search_query + '&post_query=' + post_query);
                }
        
            });

            // Quand on récupère la réponse d'AJAX
            ajax3.onload = function() {
                var json_list = JSON.parse(ajax3.responseText);
                awesomplete_field3.list = json_list; // Mis à jour de la list Awesomplete
                awesomplete_field3.evaluate(); // et on préviens Awesomplete qu'elle l'a été (updated)
            };

            // Récup de l'ID du post autocompleté et envoi dans un input caché
            var ajaxHidden3 = new XMLHttpRequest();
            
            var awesomplete_list3 = document.getElementById('awesomplete_list_3');
            var hidden_field3 = mesDivs[2].firstElementChild;
            awesomplete_list3.addEventListener('click', () => {
                var autocompleted_field = autocomplete_field3.value;
            
                ajaxHidden3.open("POST", autocomplete_hidden_id.ajax_url);
                ajaxHidden3.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                ajaxHidden3.send('action=autocompleted_id&post_title=' + autocompleted_field); 
            });
        
            ajaxHidden3.onload = function() {
                var json_response = JSON.parse(ajaxHidden3.responseText);
                hidden_field3.value = json_response;
            };
    }
/**   VERSION 1.0 : NE FONCTIONNE QU'AVEC UN SEUL INPUT.
 * 
    var autocomplete_field = document.getElementById(autocomplete_id);
    var awesomplete_field = new Awesomplete(autocomplete_field);    
    var min_letters = parseInt(monInput.dataset.minLetters); // combien de lettres avant que la requête AJAX se lance.
    var post_query = monInput.dataset.postQuery;

    autocomplete_field.addEventListener('keyup', () => {
        
        var search_query = autocomplete_field.value;
        if(search_query.length >= min_letters) {
            // On lance la requête AJAX
            ajax.open("POST", kc_autocomplete_script.ajax_url);
            ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajax.send('action=autocomplete_data&search_query=' + search_query + '&post_query=' + post_query);
        }

    });


    // Quand on récupère la réponse d'AJAX
    ajax.onload = function() {
        var json_list = JSON.parse(ajax.responseText);
        awesomplete_field.list = json_list; // Mis à jour de la list Awesomplete
        awesomplete_field.evaluate(); // et on préviens Awesomplete qu'elle l'a été (updated)
    };


    // Récup de l'ID du post autocompleté et envoi dans un input caché
    var ajax2 = new XMLHttpRequest();

    var awesomplete_list = document.getElementById('awesomplete_list_1');
    var hidden_field = document.getElementById('hidden-field');
    awesomplete_list.addEventListener('click', () => {
        var autocompleted_field = autocomplete_field.value;

        ajax2.open("POST", autocomplete_hidden_id.ajax_url);
        ajax2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax2.send('action=autocompleted_id&post_title=' + autocompleted_field); 
    });

    ajax2.onload = function() {
        var json_response = JSON.parse(ajax2.responseText);
        hidden_field.value = json_response;
    };

*/

});